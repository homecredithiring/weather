# Getting Started this Project Exam

### Get Weather API data and Aggregate the result
For further API weather documentation :

* [Open Weather Documentation](https://openweathermap.org/current)

Use this API key for do request for weather data:

> <strong>18cede6b73a127ed47992ee400a1d2bd<strong>

API Sample request by City ID

Request :

* https://api.openweathermap.org/data/2.5/weather?id=1642907&appid=18cede6b73a127ed47992ee400a1d2bd

Response : 

> {
    "coord": {
      "lon": 106.86,
      "lat": -6.22
    },
    "weather": [
      {
        "id": 721,
        "main": "Haze",
        "description": "haze",
        "icon": "50n"
      }
    ],
    "base": "stations",
    "main": {
      "temp": 298.58,
      "pressure": 1009,
      "humidity": 88,
      "temp_min": 298.15,
      "temp_max": 299.26
    },
    "visibility": 2400,
    "wind": {
      "speed": 1.5,
      "deg": 240
    },
    "clouds": {
      "all": 20
    },
    "dt": 1557786302,
    "sys": {
      "type": 1,
      "id": 9384,
      "message": 0.0074,
      "country": "ID",
      "sunrise": 1557788028,
      "sunset": 1557830648
    },
    "id": 1642907,
    "name": "Daerah Khusus Ibukota Jakarta",
    "cod": 200
  }

## SCENARIO

> The project will provide API call to Customer which provide the Weather Data from https://openweathermap.org/appid.
>
> List of the API provided to Customer :
> * GetWeatherDataByID
> * GetWeatherDataByZipCode
> * GetWeatherDataByCityName
>
> All data will be requested to https://openweathermap.org/appid


## TASKS

1. Create API Request to https://openweathermap.org/appid as needed
2. Complete all the function from WeatherController(REST API for Customer), Create service layer as needed.
3. Provide WebService data (SOAP server) for all Customer API (GetWeatherDataByID,GetWeatherDataByZipCode,GetWeatherDataByCityName),
   you can use weather.xsd in resource/xsd as your reference or you can create your own XSD
4. Create all unit testing for the function
5. Create Stress and Load Test to All Customer API (nice to have)
6. Create JMS server provider for Weather Data (nice to have)
7. Securing the Customer API (nice to have)

### Note :
> You can use memory database h2 if you want to implement database





