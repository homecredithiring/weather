package com.hcid.hiring.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/v1/data")
public class WeatherController {

    // Complete this function as requested
    // get API CALL data from openweathermap.org
    public ResponseEntity<?> getWeatherDataByID(){

        return new ResponseEntity("OK", HttpStatus.OK);
    }

    // Complete this function as requested
    // get API CALL data from openweathermap.org
    public ResponseEntity<?> getWeatherDataByZipCode(){

        return new ResponseEntity("OK", HttpStatus.OK);
    }

    // Complete this function as requested
    // get API CALL data from openweathermap.org
    public ResponseEntity<?> getWeatherDataByCityName(){

        return new ResponseEntity("OK", HttpStatus.OK);
    }
}
